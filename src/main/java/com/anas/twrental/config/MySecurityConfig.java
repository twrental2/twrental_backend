package com.anas.twrental.config;

import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.management.HttpSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import java.util.List;

@KeycloakConfiguration
public class MySecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

    private final String [] userEndPoints = {
           // "/api/v1/cars",
            "/api/v1/ordercar",
            "/api/v1/updateorder",
        //    "/api/v1/myorders",
            "/api/v1/exchange"};

    private final String [] adminEndPoints = {
          // "/api/v1/cars",
            "/api/v1/customers" ,
            "/api/v1/addcar",
            "/api/v1/deletecar",
            "/api/v1/updatecar",
            "/api/v1/cancelorder"
    };

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth){
        SimpleAuthorityMapper grantedAuthorityMapper = new SimpleAuthorityMapper();
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(grantedAuthorityMapper);
        auth.authenticationProvider(keycloakAuthenticationProvider);

    }


    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    @Bean
    @Override
    @ConditionalOnMissingBean(HttpSessionManager.class)
    protected HttpSessionManager httpSessionManager() {
        return super.httpSessionManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http
                .authorizeRequests( auth -> auth
                        .antMatchers(("/api/v1/cars")).permitAll()
                        .antMatchers(("/api/v1/myoreders")).permitAll()
                        .antMatchers(adminEndPoints).hasRole("ADMIN")
                        .antMatchers(userEndPoints).hasRole("USER")
                        .anyRequest().authenticated())
                .cors(c->{
                    CorsConfigurationSource cs = request -> {
                        CorsConfiguration cc = new CorsConfiguration();
                        cc.setAllowedOrigins(List.of("http://127.0.0.1:5500", "http://localhost:5500",
                                "http://127.0.0.1:5501","http://localhost:5501","http://127.0.0.1:5502","http://localhost:5502","http://127.0.0.1:5503","http://localhost:5503"));
                        cc.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE"));
                        cc.setAllowedHeaders(List.of("Authorization", "Content-type", "Access-Control-Allow-Origin", "accept", "brand", "name", "contractId", "newDate",
                                "userName","id"));
                        return  cc;
                    };
                    c.configurationSource(cs);
                })
                //.csrf().disable();    // without this we can make just GET request
                .csrf( csrf->{
                    csrf.ignoringAntMatchers("/h2/**");
                    csrf.disable();
                });



    }


}
