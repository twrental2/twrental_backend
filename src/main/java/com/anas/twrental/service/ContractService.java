package com.anas.twrental.service;

import com.anas.twrental.dto.IncomingOrder;
import com.anas.twrental.exception.AvailableException;
import com.anas.twrental.exception.IdException;
import com.anas.twrental.models.Car;
import com.anas.twrental.models.Contract;
import com.anas.twrental.models.Customer;
import com.anas.twrental.repository.CarRepo;
import com.anas.twrental.repository.ContractRepo;
import com.anas.twrental.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ContractService {

    @Autowired
    private ContractRepo contractRepo;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CarService carService;

    @Autowired private CarRepo carRepo;
    @Autowired private CustomerRepo customerRepo;

    // to get order
    public Contract getOrder(Long id) {
        // if(id == null)  throw new IdException();   // we can not reach this line cause id can not be null like String in postman
        return contractRepo.findById(id).orElseThrow(() -> new IdException(id, Contract.class.getSimpleName()));
    }


    // to make an order
    public Contract makeOrder(String cust_name, String brand) {
       Customer customer = customerService.getCustomerByName(cust_name);
        Car car = carService.getCarByBrand(brand);
        if (car.isAvailable()) {
            Contract contract = new Contract(customer, car);
            car.setAvailable(false);
            customer.getContractList().add(contract);

            if (!customerService.customerList.contains(customer))
                customerService.customerList.add(customer);

            contractRepo.save(contract);
            return contract;
        } else
            throw new AvailableException(brand, Car.class.getSimpleName());
    }


    // to update order it meant cancel order or renew it with same or another car
    public Contract updateOrder(Long contractId, LocalDateTime newDate) {
        Contract contract = contractRepo.findById(contractId).orElse(null);
        if (contract != null) {
            contract.setCreateAt(newDate);
        }
        return contract;
    }

    // to update order it meant cancel order or renew it with same or another car
//    public Contract updateOrder(Long id, boolean flag, String brand) {
//        Contract savedContract = getOrder(id);
//        Contract newContract = null;
//        Car contractCar = savedContract.getCar();
//        Customer contractCustomer = savedContract.getCustomer();
//
//        if (savedContract.isActive()) {
//            if (!flag) {
//                savedContract.setActive(false);
//                contractCar.setAvailable(true);
//                savedContract.setCar(null);
//            } else {
//                if (contractCar.getBrand().equals(brand))
//                    savedContract.setCreateAt(LocalDateTime.now());
//                else {
//                    savedContract.setActive(false);
//                    contractCar.setAvailable(true);
//                    savedContract.setCar(null);
//                    newContract = makeOrder(contractCustomer.getName(), brand);
//                    return contractRepo.save(newContract);
//                }
//            }
//        } else
//            throw new IdException(id);
//
//        return contractRepo.save(savedContract);
//    }

    // to cancel just an active order
    public Contract cancelOrder(Long id) {
        Contract savedContract = getOrder(id);
        if (savedContract.isActive()) {
            Car car = savedContract.getCar();
            car.setAvailable(true);
            // carService.addCar(car);
            savedContract.setActive(false);
            savedContract.setCar(null);
            contractRepo.save(savedContract);
            return savedContract;
        }
        throw new IdException(id);     //   exception ---> "order is already canceled."
    }
}
