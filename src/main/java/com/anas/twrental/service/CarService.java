package com.anas.twrental.service;
import com.anas.twrental.dto.CarWithEuro;
import com.anas.twrental.exception.AvailableException;
import com.anas.twrental.exception.NameException;
import com.anas.twrental.models.Car;
import com.anas.twrental.models.Contract;
import com.anas.twrental.repository.CarRepo;
import com.anas.twrental.repository.ContractRepo;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarService {

    @Autowired
    private CarRepo carRepo;

    @Autowired
    private ContractRepo contractRepo;


    // to get all cars
    public List<Car> getAllCars() {
        return carRepo.findAll();
    }

    // to get just available car which can be rent
    public List<Car> getAvailableCars() {
        List<Car> availableCars = new ArrayList<>();
        for (Car car : getAllCars()) {
            if (car.isAvailable())
                availableCars.add(car);
        }
        return availableCars;
    }

    // to get a car by its brand
    public Car getCarByBrand(String brand) {
        return carRepo.findCarByBrand(brand).orElseThrow(() -> new NameException(brand, Car.class.getSimpleName()));

    }

    // to add car to car repository
    public Car addCar(Car car) {
        return carRepo.save(car);
    }


    //to delete a car
    public void deleteCar(String brand) {
        Car saveCar = getCarByBrand(brand);
        // if the car is not available, so we must decouple the car form its contract and cancel the contract
        // and then delete the car
        if (!saveCar.isAvailable()) {
            Optional<Contract> savedContract = contractRepo.findByCarBrand(brand);
            savedContract.get().setActive(false);
            savedContract.get().setCar(null);
            contractRepo.save(savedContract.get());
        }

        carRepo.delete(saveCar);
    }

    // to update car
    public Car updateCar(String brand, Car car) {
        Car savedCar = getCarByBrand(brand);
        while(savedCar.isAvailable()) {            // just available cars can be updated
            savedCar.setId(savedCar.getId());     // to keep the same id
            savedCar.setBrand(car.getBrand());
            savedCar.setModel(car.getModel());
            savedCar.setAvailable(car.isAvailable());
            savedCar.setPrice_day(car.getPrice_day());
            return carRepo.save(savedCar);


        }
        throw new AvailableException(brand,Car.class.getSimpleName());
    }


    // to get Json object from a web service, this object consist of many property, but we want just result field
    // to take the price in euro
    public CarWithEuro exchangePrice(String brand) throws IOException {
        Car carByBrand = getCarByBrand(brand);
        double amount = carByBrand.getPrice_day();
        final String URL = String.format("https://api.apilayer.com/exchangerates_data/convert?to=EUR&from=SEK&amount=%s", amount);
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder()
                .url(URL)
                .addHeader("apikey", "fI6KiAYtmzmmRjAM8mYBbtjqoaTzIFus")
                .method("GET", null)
                .build();
        Response response = client.newCall(request).execute();
        //System.out.println(response.body().string());    // we dont need it

       // ResponseBody body = response.body();
        JSONObject object = new JSONObject(response.body().string());   // create Json object String
        Double price_Euro = (Double) object.get("result");        // using get() to get specific property from json object
        return new CarWithEuro(carByBrand, price_Euro);           // return car object with euro price
    }


}
