package com.anas.twrental.service;

import com.anas.twrental.exception.NameException;
import com.anas.twrental.models.Contract;
import com.anas.twrental.models.Customer;
import com.anas.twrental.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepo customerRepo;

    List<Customer> customerList = new ArrayList<>();    // just the customer who made an order

    // to get a customer by name
    // if the user did not give a name ---> get exception (you should give a name)
    // if the user give name not found in db ----> get exception( this name not found in this specific db

    public Customer getCustomerByName(String name) {
        return customerRepo.findCustomersByName(name).orElseThrow(() -> new NameException(name, Customer.class.getSimpleName()));  //'''''''
    }

    // to get all customer's contacts (my orders)
    public List<Contract> getAllCustomerContract(String name) {
        Customer savedCustomer = getCustomerByName(name);
        return savedCustomer.getContractList();
    }

    // to get all customers just who have contract
    public List<Customer> getAllCustomers() {
        return customerList;
    }


    public Customer addCustomer(Customer customer) {
        return customerRepo.save(customer);
    }


}
