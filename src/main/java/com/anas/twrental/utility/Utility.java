package com.anas.twrental.utility;

import com.anas.twrental.models.Address;
import com.anas.twrental.models.Car;
import com.anas.twrental.models.Customer;
import com.anas.twrental.repository.AddressRepo;
import com.anas.twrental.repository.CarRepo;
import com.anas.twrental.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class Utility implements CommandLineRunner {

    @Autowired
    private CarRepo carRepo;
    @Autowired
    private AddressRepo addressRepo;
    @Autowired
    private CustomerRepo customerRepo;

    @Bean
    public PasswordEncoder encoder(){
        return NoOpPasswordEncoder.getInstance();
    }


    @Override
    public void run(String... args) {


        Car car1 = new Car("audi","a3",100);
        Car car2 = new Car("BMV","300",200);
        Car car3 = new Car("merse","A_class",300);
        Car car4 = new Car("volvo","c60",400);
        Car car5 = new Car("opel","z2",50);

        carRepo.saveAll(Arrays.asList(car1,car2,car3,car4,car5));

        Address a1 = new Address("Stockholm", "vasagatan", 10);
        Address a2 = new Address("Kista", "gårdsgatan", 15);
        Address a3 = new Address("Solna", "solnaC", 30);
        Address a4 = new Address("märsta", "malmvägen", 40);
        Address a5 = new Address("sollentuna", "bagarby", 50);

        addressRepo.saveAll(Arrays.asList(a1,a2,a3,a4,a5));


        Customer c1 = new Customer("Anas", "anas", "1234",  "ADMIN" , null); // the ADMIN
        Customer c2 = new Customer("teim", "teim", "1234",  "USER",null);
        Customer c3 = new Customer("Yusuf", "yusuf", "1234",  "USER",null);
        Customer c4 = new Customer("Fahad", "fahad", "1234",  "USER", null);
        Customer c5 = new Customer("Tania", "tania", "1234",  "USER", null);

        customerRepo.saveAll(Arrays.asList(c1,c2,c3,c4,c5));

        c1.setAddress(a1);
        c2.setAddress(a2);
        c3.setAddress(a3);
        c4.setAddress(a4);
        c5.setAddress(a5);

        customerRepo.saveAll(Arrays.asList(c1,c2,c3,c4,c5));

    }


}
