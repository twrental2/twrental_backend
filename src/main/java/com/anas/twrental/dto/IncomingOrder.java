package com.anas.twrental.dto;

import java.time.LocalDate;

public class IncomingOrder {
    private String userName;
    private Long carId;
    private LocalDate bookingDate;

    public IncomingOrder(String userName, Long carId, LocalDate bookingDate) {
        this.userName = userName;
        this.carId = carId;
        this.bookingDate = bookingDate;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDate bookingDate) {
        this.bookingDate = bookingDate;
    }
}
