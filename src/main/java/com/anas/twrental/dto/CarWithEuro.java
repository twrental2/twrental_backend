package com.anas.twrental.dto;

import com.anas.twrental.models.Car;

public class CarWithEuro {    // this is the object we send to know the car price in euro

    private  String brand ;
    private  String model ;
    private  double price_day;
    private  double price_euro;      // this field we take it from a web service
    private boolean available;

    public CarWithEuro(Car car, double price_euro) {
        // all the fields is the same just we add the price in euro
        this.brand = car.getBrand();
        this.model = car.getModel();
        this.price_day = car.getPrice_day();
        this.price_euro = price_euro;
        this.available = car.isAvailable();
    }

    public CarWithEuro() {
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getPrice_day() {
        return price_day;
    }

    public double getPrice_euro() {
        return price_euro;
    }

    public boolean isAvailable() {
        return available;
    }
}
