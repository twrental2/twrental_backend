package com.anas.twrental.controller;

import com.anas.twrental.dto.CarWithEuro;
import com.anas.twrental.models.Car;
import com.anas.twrental.models.Contract;
import com.anas.twrental.service.CarService;
import com.anas.twrental.service.ContractService;
import com.anas.twrental.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserController {

    @Autowired
    private CarService carService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ContractService contractService;

    // to get all available cars
    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getAvailableCars(){
        return new ResponseEntity<>(carService.getAvailableCars(), HttpStatus.FOUND);
    }

    // make an order by customer's and car's name
    @PostMapping("/ordercar")
    public ResponseEntity<Contract> makeOrder(@RequestHeader String name, @RequestHeader String brand ){
        return new ResponseEntity<>(contractService.makeOrder(name , brand),HttpStatus.OK);
    }

    // to update an order
    @PutMapping("/updateorder")
    public ResponseEntity<Contract>  updateOrder(@RequestHeader Long contractId ){
        return new ResponseEntity<>(contractService.updateOrder(contractId, LocalDateTime.now()), HttpStatus.OK);
    }

    // to update an order
    @PutMapping("/testupdate")
    public ResponseEntity<String>  updateOrder(){
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }


    // to get all customer's contract
    @GetMapping("/myorders")
    public ResponseEntity<List<Contract>> getMyOrders(@RequestHeader String name){
        return new ResponseEntity<>(customerService.getAllCustomerContract(name), HttpStatus.OK);
    }

    // to get the car price in euro by using web services
    @GetMapping("/exchange")
    public ResponseEntity<CarWithEuro> getExchange(@RequestHeader String brand) throws IOException {
        return new ResponseEntity<>( carService.exchangePrice(brand),HttpStatus.OK);
    }

/*
    //  extra
    @GetMapping("/get/customer")
    public ResponseEntity<Customer> getCustomerByName(@RequestHeader String name){
        return new ResponseEntity<>(customerService.getCustomerByName(name), HttpStatus.OK);
    }

    // extra
    @GetMapping("/get/car")
    public ResponseEntity<Car> getCarByName(@RequestHeader String brand){
        return new ResponseEntity<>(carService.getCarByBrand(brand), HttpStatus.OK);
    }

    // extra
    @GetMapping("/get/order")
    public ResponseEntity<Contract> getOrder(@RequestHeader Long  id){
        return new ResponseEntity<>(contractService.getOrder(id), HttpStatus.FOUND);
    }

 */




}
