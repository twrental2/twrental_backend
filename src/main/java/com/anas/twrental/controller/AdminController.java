package com.anas.twrental.controller;

import com.anas.twrental.models.Car;
import com.anas.twrental.models.Contract;
import com.anas.twrental.models.Customer;
import com.anas.twrental.service.CarService;
import com.anas.twrental.service.ContractService;
import com.anas.twrental.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class AdminController {

    @Autowired
    private CarService carService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ContractService contractService;


    // to get all customer(just who made contract),so if the same customer made many contracts, he
    // will be counted just one customer
    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getAllCustomers(){
        return new ResponseEntity<>(customerService.getAllCustomers(), HttpStatus.OK);
    }

    // to add car to car list
    @PostMapping("/addcar")
    public ResponseEntity<Car> addCar(@RequestBody Car car){
        return new ResponseEntity<>(carService.addCar(car),HttpStatus.CREATED);
    }

    // to deleter car from list (if it is available or not)
    @DeleteMapping("/deletecar")
    public ResponseEntity<String > deleteCar(@RequestHeader String brand){
        carService.deleteCar(brand);
        return new ResponseEntity<>(String.format("car %s is deleted.",brand), HttpStatus.OK);
    }

    // to update car
    @PutMapping("/updatecar")
    public ResponseEntity<Car> updateCar(@RequestHeader String brand, @RequestBody Car car){
        return new ResponseEntity<>(carService.updateCar(brand, car), HttpStatus.ACCEPTED);
    }

    // to cancel order and make it inactive
    @PutMapping("/cancelorder")
    public ResponseEntity<Contract> cancelOrder(@RequestHeader Long id){
        return new ResponseEntity<>(contractService.cancelOrder(id), HttpStatus.ACCEPTED);
    }




}
