package com.anas.twrental.repository;

import com.anas.twrental.models.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CarRepo extends JpaRepository<Car, Long> {


    Optional<Car> findCarByBrand(String brand);


}


