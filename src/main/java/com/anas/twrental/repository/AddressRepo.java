package com.anas.twrental.repository;

import com.anas.twrental.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepo extends JpaRepository<Address, Long> {
}


