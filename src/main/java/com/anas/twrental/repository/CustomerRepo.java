package com.anas.twrental.repository;

import com.anas.twrental.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepo extends JpaRepository<Customer, Long> {

   Optional<Customer> findCustomersByName(String name);
}


