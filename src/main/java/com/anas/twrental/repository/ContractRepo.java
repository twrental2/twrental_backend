package com.anas.twrental.repository;

import com.anas.twrental.models.Contract;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ContractRepo extends JpaRepository<Contract, Long> {

    Optional<Contract> findByCarBrand(String  brand);
}


