package com.anas.twrental.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    private String city;
    private String street;
    private int post_no;

    public Address() {
    }

    public Address(Long id, String city, String street, int post_no) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.post_no = post_no;
    }

    public Address(String city, String street, int post_no) {
        this.city = city;
        this.street = street;
        this.post_no = post_no;
    }

    // getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getPost_no() {
        return post_no;
    }

    public void setPost_no(int post_no) {
        this.post_no = post_no;
    }

    // hashCode and equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        return Objects.equals(id, address.id);
    }

    @Override
    public int hashCode() {
        int result = city != null ? city.hashCode() : 0;
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + post_no;
        return result;
    }

    // toString
    @Override
    public String toString() {
        return "Address: " +
                "city= " + city + '\'' +
                "street= " + street + '\'' +
                "post_no= " + post_no;

    }
}
