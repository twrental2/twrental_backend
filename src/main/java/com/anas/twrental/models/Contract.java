package com.anas.twrental.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean isActive = true;

    @CreationTimestamp
    @Column(name = "order_date", nullable = false )   // we should put update = false, but we have just these fields
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy hh:mm:ss")
    private LocalDateTime createAt;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Customer customer;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER )
    private Car car;

    public Contract() {
    }

    public Contract(Long id,  boolean isActive, LocalDateTime createAt, Customer customer, Car car) {
        this.id = id;
        this.isActive = isActive;
        this.createAt = createAt;
        this.customer = customer;
        this.car = car;
    }

    // we invoke this contractor when we create an order
    public Contract( Customer customer, Car car) {
        setCreateAt(LocalDateTime.now());
        this.isActive = true;
        this.customer = customer;
        this.car = car;
    }


    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contract contract = (Contract) o;

        return Objects.equals(id, contract.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "id=" + id +
                ", isActive=" + isActive +
                ", createAt=" + createAt +
                ", customer=" + customer +
                " car: " + car +
                '}';
    }
}
