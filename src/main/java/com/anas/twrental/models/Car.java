package com.anas.twrental.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private  String brand ;
    private  String model ;
    private  double price_day;
    private boolean available = true;

    /*
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER , mappedBy = "car")
    private Contract contract;

     */

    public Car() {
    }

    public Car(Long id, String brand, String model, double price_day, boolean available ) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.price_day = price_day;
    }

    public Car( String brand, String model, double price_day ){
        this.brand = brand;
        this.model = model;
        this.price_day = price_day;
        this.available = true ;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice_day() {
        return price_day;
    }

    public void setPrice_day(double price_day) {
        this.price_day = price_day;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    /*
    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return Objects.equals(id, car.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", price_day=" + price_day +
                ", available=" + available +
               // ", contract=" + contract +
                '}';
    }
}
