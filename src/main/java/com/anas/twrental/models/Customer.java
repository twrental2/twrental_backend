package com.anas.twrental.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name ;
    private String username ;
    private String password ;
    private String role;

    @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER)
    private Address address;

    @JsonIgnore    // otherwise, we get Recursion error
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Contract> contractList = new ArrayList<>();

    public Customer() {}

    public Customer(String name, String username, String password, String role, Address address) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.role = role;
        this.address = address;
    }

    public Customer(Long id , String name, String username, String password, String role, Address address, List<Contract> contractList) {
        this.id = id ;
        this.name = name;
        this.username = username;
        this.password = password;
        this.role = role;
        this.address = address;
        this.contractList = contractList;
    }


    public void  addContract(Contract contract){     // to add a contract to customer listContract
       this.contractList.add(contract);
    }

    public void  removeContract(Contract contract){      // to remove a contract
        this.contractList.remove(contract);
    }

    public List<Contract> getContractList() {
        return contractList;
    }

    public void setContractList(List<Contract> contractList) {
        this.contractList = contractList;
    }

    // getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    // hashCode and equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return Objects.equals(id, customer.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    // toString

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", address=" + address +
                ", userRole=" + role +
                "contractList :"+ contractList +
                '}';
    }
}
