package com.anas.twrental.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import java.time.LocalDateTime;

@ControllerAdvice
public class RentalExceptionHandel {

    @ExceptionHandler(RentalException.class)
    public ResponseEntity<Object> handleTWRentalExceptions(RentalException e) {
        Error error = new Error(e.getMessage(), LocalDateTime.now());

        return new ResponseEntity<>(error, e.getHttpStatus());
    }

}
