package com.anas.twrental.exception;

import org.springframework.http.HttpStatus;

public abstract class RentalException extends RuntimeException{

    private String message ;
    private HttpStatus httpStatus ;

    public RentalException(){

    }
    public RentalException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getMessage(){
        return this.message;
    };
    public HttpStatus getHttpStatus(){
        return this.httpStatus;
    };
}


