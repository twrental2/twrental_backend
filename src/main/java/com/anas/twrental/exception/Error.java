package com.anas.twrental.exception;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;


public class Error {

    private String message;

    // this to format the date String in Json response
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy  hh:mm:ss")
    private LocalDateTime timestamp;


    public Error(String message, LocalDateTime timestamp) {
        this.message = message;
        this.timestamp = timestamp;
    }


    // getters and setters
    public String getMessage() {
        return message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTimestamp(LocalDateTime timestamp) {
       // this.timestamp = LocalDateTime.now();
       this.timestamp = timestamp;
    }


    // toString
    @Override
    public String toString() {
        return "Error: " +
                "message= " + message + '\'' +
                "timestamp= " + timestamp +
                '}';
    }
}
