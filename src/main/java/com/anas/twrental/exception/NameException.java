package com.anas.twrental.exception;

import org.springframework.http.HttpStatus;

public class NameException extends RentalException {

    private String name;
    private String className;

    public NameException(String name ,String className) {
        this.name = name;
        this.className = className;
    }

    @Override
    public String getMessage() {
        if(this.name.isEmpty()) return "you must give a name";
        return String.format("%s is not found in %s table.",name, className);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }
}
