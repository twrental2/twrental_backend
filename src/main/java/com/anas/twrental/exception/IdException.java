package com.anas.twrental.exception;

import org.springframework.http.HttpStatus;

public class IdException extends RentalException {
    private Long order_id;
    private String className;

    public IdException(Long id ) {
        super(String.format("order with id %s is already canceled.", id ), HttpStatus.NOT_FOUND);
        //this.order_id = -1L;
       // alreadyCanceled();

    }

    public IdException( ) {
        super("this contract does not belong to you.", HttpStatus.NOT_FOUND);
        this.order_id = -1L;
    }

    public IdException(Long order_id, String className) {   // work ok
        super(String.format("order with id %d is not found in %s table.", order_id, className), HttpStatus.NOT_FOUND);
        this.order_id = order_id;
        this.className = className;
    }

/*
    public String alreadyCanceled() {
        return ("order is already canceled.");
    }

 */

    /*
    public String notYourContract(Long order_id) {
        return (String.format("order with id %d is not in your contract list.", order_id));
    }



    public Long getOrder_id() {
        return order_id;
    }

    public String getClassName() {
        return className;
    }

     */

/*    @Override
    public String getMessage() {

        if (this.order_id == null) return "you must give an id order.";
        //return String.format("order with id %d is not found in %s table.", order_id, className);
        return super.getMessage();
    }*/


   /* @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }*/
}







