package com.anas.twrental.exception;

import org.springframework.http.HttpStatus;

public class AvailableException extends RentalException {
    private String brand;
    private String className;

    public AvailableException(String brand, String className) {
        this.brand = brand;
        this.className = className;
        //getMessage();
    }

    public String getBrand() {
        return brand;
    }

    public String getClassName() {
        return className;
    }

    @Override
    public String getMessage() {
        if (this.brand.isEmpty()) return "you must give a brand name";   // if user didn't give name of car
        return String.format("%s is not available in %s table.", brand, className);   // if user gave not an available car
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_ACCEPTABLE;
    }
}
